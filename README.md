# goeznacl

goeznacl is an MIT-licensed Go library for making work with cryptography easier by providing an easy-to-use API for public key encryption, secret key encryption, digital signatures, data hashing, and password hashing. It was originally written strictly as a wrapper around the NaCl implementation provided in the Go main libraries, but it has since added support for other algorithms beyond those provided by NaCl and retains the 'goeznacl' name for kicks and giggles.

## Description

Cryptography is really hard. Any code which implements it is equally hard. Anything which touches the implementation code isn't much easier. This library came from a need to work with crypto keys over a text-based protocol, but as it matured, I discovered it also made debugging code which interacts with cryptography much easier, too.

**Please** don't use this code to place *important* crypto keys in your code or embed backdoors. No one needs that kind of drama.

## Contributing and Support

Although a mirror repository can be found on GitHub for historical reasons, the official repository for this project is on [GitLab](https://gitlab.com/darkwyrm/goeznacl). Please submit issues and pull requests there.

## Usage

The code itself is well-commented, and usage should be pretty obvious, but if you want a good 10,000 foot view, read on. A full reference of the library can be found [here](https://pkg.go.dev/gitlab.com/darkwyrm/goeznacl#section-documentation).

### CryptoString

For starters, a new data type, CryptoString, is used heavily when interacting with this library. In short, CryptoStrings are Base85-encoded binary data -- usually hashes or crypto keys -- that have an algorithm name prepended and a colon separating the two. Nothing earth-shattering, but it makes a genuine difference. It's a means to a goal, so here's how you use it:

- To make one from existing data, call `NewCSFromBytes()`.
- `NewCS()` is for making one from a string containing CryptoString data
- `AsString()` returns prefix and encoded data as a string. `AsBytes()` returns the same thing as a byte slice.
- `AsRaw()` returns the raw, unencoded binary data and no prefix

### Encryption, Decryption, and Digital Signatures

Even with goeznacl, encryption isn't just a matter of waving the magic Encryption Wand, but it's pretty close. Go look at [examples_test.go](https://gitlab.com/darkwyrm/goeznacl/-/blob/master/examples_test.go) for some examples of the process using goeznacl with lots of detailed comments for help. Keep in mind that how cryptography is applied is as important as choosing the correct algorithms.

### Hashing

A cryptographic hash is a string of bytes which, in theory, uniquely represent a chunk of data. Hashes can be used to identify data without giving the data itself away, so a hash can be used to identify a secret key without compromising the key itself, for example. The hashing capabilities built into goeznacl operate from the assumption that what you're hashing will fit into RAM. If this isn't the case, you'll want to interact with the hashing libraries directly and you can use the sources for [`GetHash()`](https://gitlab.com/darkwyrm/goeznacl/-/blob/master/hash.go) for some direction on how to do this.

**NOTE:** If you are looking to hash passwords, DO NOT use the regular hashing facilities. Instead, jump down to the next section on password hashing.

```go
import (
	"fmt"
	ezn "gitlab.com/darkwyrm/goeznacl"
)

func main() {
	mySecretKey := ezn.GenerateSecretKey(ezn.PreferredSecretType())
	keyHash, err := ezn.GetHash(ezn.PreferredHashType(), mySecretKey.Key.AsBytes())
	if err != nil {
		panic(err)
	}

	fmt.Printf("My Secret Key:\n%s\n", mySecretKey.Key.AsString())
	fmt.Printf("Hash of my secret key:\n%s\n", keyHash.AsString())
}
```
The output of this little example looks something like this:

```
My Secret Key:
AES-256:VAv|e5|}hdcWs)<zF32m%Yj8OMACApmv_yKeV=wl
Hash of my secret key:
BLAKE3-256:5M{w<@~<RG`qt_J10+Aw1+Mtg#Os9*p|o$6(b+a1
```

For those more familiar with working with hashing algorithms, you might be a bit suprised the `BLAKE3-256` prefix. The original BLAKE algorithm was a runner up to what became the SHA3 algorithm and BLAKE3 is a couple iterations later on the original. BLAKE3 many times faster than SHA256 and provides 128 bits of security, which is enough for most needs. Other algorithms, such as SHAKE and SHA2 are available. `GetHashAlgorithms()` will return a list of all available hashing algorithms supported by the library, including `SHA-256` and `SHA-512`.

### Password Hashing

Unlike regular hashing, which is designed to be fast, password hashing is similar except that the hashing algorithm is designed to be resource intensive to prevent bad actors from breaking a hash by throwing a GPU cluster at it. Although some algorithms out there, such as scrypt and bcrypt, are often used for hashing passwords, these methods just run regular hash algorithms ten or hundreds of thousands of times to make things harder. goeznacl uses another: [Argon2id](https://github.com/P-H-C/phc-winner-argon2), an algorithm that came out of the [Password Hashing Competition](https://www.password-hashing.net/) from the 2010s.

Hashing a password with goeznacl is very straightforward: `HashPassword()` takes a string containing a password creates an Argon2id hash, and `VerifyPasswordHash()` takes a string containing a password and another string containing a password hash and returning true if they match. Argon2id has a lot of options for tuning performance; `HashPassword()` chooses a set of values which should work well for most user-facing situations. If this is not acceptable, [use the source for `HashPassword()`](https://gitlab.com/darkwyrm/goeznacl/-/blob/master/password.go) for some guidance on how to use the argon2 module directly.

### Other Algorithms and Certified Algorithm Support

Some use cases will require support for specific or multiple algorithms. For these needs are the functions `GetAsymmetricAlgorithms()`, `GetSecretAlgorithms()`, `GetSigningAlgorithms()`, and `GetHashAlgorithms()`, which return a slice of strings containing the names of all algorithms supported. There are corresponding calls `PreferredAsymmetricType()`, `PreferredSecretType()`, `PreferredSigningType()`, and `PreferredHashType()` for instances where the library chooses for you if you're not sure which one is best.

For situations where certified algorithms are required for compliance, `RequireCertifiedAlgorithms(true)` will exclude algorithms which are not certified. This library uses the U.S. government's Federal Information Processing Standards (FIPS) for choosing algorithms. This is not a guarantee of any kind of compliance, only an attempt to assist, as compliance almost always requires careful application of compliant algorithms and more hoops to jump through than the Barnum and Bailey Circus. Confirming compliance in your area will require consulting official government resources.

As of this writing, goeznacl supports the following algorithms:

- Symmetric (Secret Key) Encryption
	- AES-128
	- AES-256*
	- XSALSA20
- Asymmetric (Public Key) Encryption
	- CURVE25519*
	- RSA2048-SHA256, 2048-bit RSA which uses SHA256 for internal hash calculations
- Digital Signatures
	- ECDSA-P256-SHA256, ECDSA which uses the P-256 elliptic curve and SHA256 for internal hash calculations
	- ED25519*
- Cryptographic Hashes
	- BLAKE2B-256
	- BLAKE2B-512
	- BLAKE3-256*
	- SHA-256
	- SHA-512
	- SHAKE-256, SHAKE128 algorithm with a 256-bit hash
	- SHAKE-512, SHAKE256 algorithm with a 512-bit hash
- Password Hashes
	- Argon2id

Algorithms with an asterisk attached to them are the algorithms returned by `Preferred*Type()` calls.
