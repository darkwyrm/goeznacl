package goeznacl

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"errors"
	"fmt"

	"gitlab.com/darkwyrm/b85"
	"golang.org/x/crypto/nacl/box"
)

// Returns a list of supported asymmetric encryption algorithms
func GetAsymmetricAlgorithms() []string {
	if CertifiedAlgorithmsRequired() {
		return []string{
			"RSA2048-SHA256",
		}
	}
	return []string{
		"CURVE25519",
		"RSA2048-SHA256",
	}
}

// EncryptionPair defines a pair of asymmetric encryption keys
type EncryptionPair struct {
	PublicHash  CryptoString
	PrivateHash CryptoString
	PublicKey   CryptoString
	PrivateKey  CryptoString
}

// NewEncryptionPair creates a new asymmetric encryption keypair from two CryptoString objects
func NewEncryptionPair(pubkey CryptoString, privkey CryptoString) (EncryptionPair, error) {
	var out EncryptionPair

	// All parameter validation is handled in Set
	if err := out.Set(pubkey, privkey); err != nil {
		return out, err
	}

	return out, nil
}

// EncryptionPairFromStrings creates a new asymmetric encryption keypair from two strings containing
// data in CryptoString format. This call is intended for situations where you know the data being
// passed is valid. If any error should occur, nil is returned instead.
func EncryptionPairFromStrings(pubstr string, privstr string) *EncryptionPair {
	var out EncryptionPair

	pubkey, err := NewCS(pubstr)
	if err != nil {
		return nil
	}

	privkey, err := NewCS(privstr)
	if err != nil {
		return nil
	}

	// All parameter validation is handled in Set
	if err := out.Set(pubkey, privkey); err != nil {
		return nil
	}

	return &out
}

// GetEncryptionType returns the algorithm used by the key
func (kpair EncryptionPair) GetEncryptionType() string {
	return kpair.PublicKey.Prefix
}

// GetType returns the type of key -- asymmetric or symmetric
func (kpair EncryptionPair) GetType() string {
	return "asymmetric"
}

// Set assigns a pair of CryptoString values to the EncryptionPair
func (kpair *EncryptionPair) Set(pubkey CryptoString, privkey CryptoString) error {

	if pubkey.Prefix != privkey.Prefix {
		return errors.New("key type mismatch")
	}

	if !isValidAsymmetricKeyType(pubkey.Prefix) || !isValidAsymmetricKeyType(privkey.Prefix) {
		return ErrUnsupportedAlgorithm
	}
	kpair.PublicKey = pubkey
	kpair.PrivateKey = privkey

	var err error
	kpair.PublicHash, err = GetHash(preferredHashAlgorithm, pubkey.AsBytes())
	if err != nil {
		return err
	}

	kpair.PrivateHash, err = GetHash(preferredHashAlgorithm, privkey.AsBytes())
	return err
}

// Generate creates a new asymmetric encryption keypair
func GenerateEncryptionPair(algorithm string) (EncryptionPair, error) {

	var out EncryptionPair

	if !isValidAsymmetricKeyType(algorithm) {
		return out, ErrUnsupportedAlgorithm
	}

	switch algorithm {
	case "CURVE25519":
		pubkey, privkey, err := box.GenerateKey(rand.Reader)
		if err != nil {
			return out, err
		}

		err = out.Set(CryptoString{"CURVE25519", b85.Encode(pubkey[:])},
			CryptoString{"CURVE25519", b85.Encode(privkey[:])})
		if err != nil {
			return out, err
		}
	case "RSA2048-SHA256":
		privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			return out, err
		}

		flatPriv, err := x509.MarshalPKCS8PrivateKey(privateKey)
		if err != nil {
			return out, err
		}

		flatPub, err := x509.MarshalPKIXPublicKey(&privateKey.PublicKey)
		if err != nil {
			return out, err
		}

		err = out.Set(CryptoString{"RSA2048-SHA256", b85.Encode(flatPub)},
			CryptoString{"RSA2048-SHA256", b85.Encode(flatPriv)})
		if err != nil {
			return out, err
		}
	default:
		return out, errors.New("bug: unhandled algorithm in GenerateEncryptionPair()")
	}

	return out, nil
}

// Encrypt encrypts a byte slice using the internal public key and returns the encrypted data as
// a CryptoString.
func (kpair EncryptionPair) Encrypt(data []byte) (CryptoString, error) {
	var empty CryptoString

	if data == nil {
		return empty, nil
	}

	pubKeyDecoded := kpair.PublicKey.RawData()
	if pubKeyDecoded == nil {
		return empty, b85.ErrDecodingB85
	}

	switch kpair.PrivateKey.Prefix {
	case "CURVE25519":
		// This kind of stupid is why this class is even necessary
		var tempPtr [32]byte
		ptrAdapter := tempPtr[0:32]
		copy(ptrAdapter, pubKeyDecoded)

		encryptedData, err := box.SealAnonymous(nil, data, &tempPtr, rand.Reader)
		if err != nil {
			return empty, err
		}
		return CryptoString{"CURVE25519", b85.Encode(encryptedData)}, nil
	case "RSA2048-SHA256":
		keyAdapter, err := x509.ParsePKIXPublicKey(pubKeyDecoded)
		if err != nil {
			return empty, fmt.Errorf("error decoding encryption key: %s", err.Error())
		}
		encKey := keyAdapter.(*rsa.PublicKey)

		ciphertext, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, encKey, data, nil)
		if err != nil {
			return empty, err
		}

		return CryptoString{"RSA2048-SHA256", b85.Encode(ciphertext)}, nil
	default:
		return empty, errors.New("bug: unhandled algorithm in Encrypt()")
	}
}

// Decrypt decrypts a CryptoString of encrypted data
func (kpair EncryptionPair) Decrypt(data CryptoString) ([]byte, error) {
	if !data.IsValid() {
		return nil, ErrInvalidCS
	}

	privKeyDecoded := kpair.PrivateKey.RawData()
	if privKeyDecoded == nil {
		return nil, b85.ErrDecodingB85
	}

	decodedData, err := b85.Decode(data.Data)
	if err != nil {
		return nil, err
	}

	switch kpair.PrivateKey.Prefix {
	case "CURVE25519":
		// The decryption interface for box is... less than ideal, to say the least.
		var privKeyPtr [32]byte
		ptrAdapter := privKeyPtr[0:32]
		copy(ptrAdapter, privKeyDecoded)

		pubKeyDecoded := kpair.PublicKey.RawData()
		if pubKeyDecoded == nil {
			return nil, b85.ErrDecodingB85
		}
		var pubKeyPtr [32]byte

		ptrAdapter = pubKeyPtr[0:32]
		copy(ptrAdapter, pubKeyDecoded)

		if decryptedData, ok := box.OpenAnonymous(nil, decodedData, &pubKeyPtr, &privKeyPtr); ok {
			return decryptedData, nil
		}
		return nil, ErrDecryptionFailure

	case "RSA2048-SHA256":
		keyAdapter, err := x509.ParsePKCS8PrivateKey(privKeyDecoded)
		if err != nil {
			return nil, fmt.Errorf("error decoding decryption key: %s", err.Error())
		}
		privKey := keyAdapter.(*rsa.PrivateKey)

		plaintext, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, privKey, decodedData, nil)
		if err != nil {
			return nil, err
		}

		return plaintext, nil

	default:
		return nil, errors.New("bug: unhandled algorithm in Decrypt()")
	}
}

// EncryptionKey contains the public half of an asymmetric encryption keypair
type EncryptionKey struct {
	PublicHash CryptoString
	PublicKey  CryptoString
}

// NewEncryptionKey instantiates an asymmetric encryption key from a CryptoString
func NewEncryptionKey(pubkey CryptoString) (EncryptionKey, error) {
	var out EncryptionKey

	if err := out.Set(pubkey); err != nil {
		return out, err
	}

	return out, nil
}

// EncryptionKeyString creates a new EncryptionKey object from a string containing data in
// CryptoString format. This call is intended to be used in situations where you are certain the
// data passed to it is valid. Errors will cause nil to be returned.
func EncryptionKeyFromString(pubstr string) *EncryptionKey {
	var out EncryptionKey

	pubCS, err := NewCS(pubstr)
	if err != nil {
		return nil
	}
	// All parameter validation is handled in Set
	if err := out.Set(pubCS); err != nil {
		return nil
	}

	return &out
}

// GetEncryptionType returns the algorithm used by the key
func (ekey EncryptionKey) GetEncryptionType() string {
	return ekey.PublicKey.Prefix
}

// GetType returns the type of key -- asymmetric or symmetric
func (ekey EncryptionKey) GetType() string {
	return "asymmetric"
}

// Set assigns a CryptoString to the instance
func (ekey *EncryptionKey) Set(pubkey CryptoString) error {

	if !isValidAsymmetricKeyType(pubkey.Prefix) {
		return ErrUnsupportedAlgorithm
	}
	ekey.PublicKey = pubkey

	var err error
	ekey.PublicHash, err = GetHash(preferredHashAlgorithm, pubkey.AsBytes())

	return err
}

// Encrypt encrypts a byte slice using the internal public key.
func (ekey EncryptionKey) Encrypt(data []byte) (CryptoString, error) {
	var empty CryptoString
	if data == nil {
		return empty, nil
	}

	pubKeyDecoded := ekey.PublicKey.RawData()
	if pubKeyDecoded == nil {
		return empty, b85.ErrDecodingB85
	}

	switch ekey.PublicKey.Prefix {
	case "CURVE25519":
		// This kind of stupid is why this class is even necessary
		var tempPtr [32]byte
		ptrAdapter := tempPtr[0:32]
		copy(ptrAdapter, pubKeyDecoded)

		encryptedData, err := box.SealAnonymous(nil, data, &tempPtr, rand.Reader)
		if err != nil {
			return empty, err
		}
		return CryptoString{"CURVE25519", b85.Encode(encryptedData)}, nil
	case "RSA2048-SHA256":
		keyAdapter, err := x509.ParsePKIXPublicKey(pubKeyDecoded)
		if err != nil {
			return empty, fmt.Errorf("error decoding encryption key: %s", err.Error())
		}
		encKey := keyAdapter.(*rsa.PublicKey)

		ciphertext, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, encKey, data, nil)
		if err != nil {
			return empty, err
		}

		return CryptoString{"RSA2048-SHA256", b85.Encode(ciphertext)}, nil
	default:
		return empty, errors.New("bug: unhandled algorithm in Encrypt()")
	}
}

// Returns true if the specified algorithm is supported by the library for public key encryption
func isValidAsymmetricKeyType(algo string) bool {
	switch algo {
	case "CURVE25519", "RSA2048-SHA256":
		return true
	}

	return false
}
