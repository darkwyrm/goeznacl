package goeznacl

import (
	"fmt"
	"strings"
	"testing"
)

func TestEncryptionExample(t *testing.T) {

	// This example demonstrates goeznacl usage for encrypting a bit of data using common
	// cryptographic patterns. In this case, we have a secret message that Bob wants to send to
	// Alice.

	bobMessage := "Failures, repeated failures, are finger posts on the road to achievement. " +
		"One fails forward toward success. -- C.S. Lewis"

	// Here we create the secret key to encode our message. Symmetric encryption, which is what
	// powers the SecretKey type, is much, much faster than asymmetric encryption. The call to
	// PreferredSecretType() gets the library's recommended secret key algorithm because we don't
	// care in this instance about how it's done, so long as it's done safely. For those who care,
	// it's AES-256. If you were to print out this key, it would look something like
	// "AES-256:fRkTfJz0FOaZ;&H13uO*a`vSSReh|IhfLSz=tBcZ"
	bobSecretKey, err := GenerateSecretKey(PreferredSecretType())
	if err != nil {
		panic(err)
	}

	// Encrypt our secret message. If you were to print out the result, it would be
	// 'random-character gibberish' much like our secret key would except that the message would be
	// much longer.
	encryptedMessage, err := bobSecretKey.Encrypt([]byte(bobMessage))
	if err != nil {
		panic(err)
	}

	// We're now going to use what the industry calls a "key encapsulation mechanism", or KEM for
	// short. An asymmetric keypair -- also known as a public key pair -- is used to encrypt the
	// secret key. We're going to assume that our friend has given us their public key some other
	// way. The NewCS() call turns the encoded key string we received from our friend and turns it
	// into the CryptoString instance that the EncryptionKey type needs.
	//
	// In normal situations outside this example, we would call GenerateEncryptionPair(), but
	// we're going to use the output of this example for the encrypted message in the decryption
	// example, so we'll use a pregenerated encryption key instead.
	aliceEncCS, err := NewCS("CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`")
	if err != nil {
		panic(err)
	}
	aliceEncKey, err := NewEncryptionKey(aliceEncCS)
	if err != nil {
		panic(err)
	}

	// Here we encrypt the secret key used for the message with our friend's encryption key.
	// Asymmetric encryption is slow, so we only use it for small bits of data like this. Note that
	// we are encrypting the CryptoString version of the key so that when our friend gets it, they
	// know what kind of encryption was used on the message.
	encryptedKey, err := aliceEncKey.Encrypt(bobSecretKey.Key.AsBytes())
	if err != nil {
		panic(err)
	}

	// And here we print the results of our efforts, ready for pasting into an e-mail or printing
	// onto a self-destructing piece of paper.
	fmt.Printf("The encrypted message key:\n%s\n\n", encryptedKey)
	fmt.Printf("Bob's secret message:\n%s\n", encryptedMessage)
}

func TestDecryptionExample(t *testing.T) {

	// This example demonstrates how Alice would decrypt and read Bob's secret message.

	// This is a sample of the output from the encryption example. Normally we would directly use
	// the encrypted data obtained a different way, but here we just instantiate a couple
	// CryptoStrings which contain the output created when running the encryption example.
	msgKeyData := CryptoString{"CURVE25519",
		"Uik5#!Dkeu5R*UZLO}UR&@vqJR5~o<iuZFg+htHcDh^$9ZVawrhY&QccoxmBj#gtz!qC*Fw" +
			"!bxfY5F|dikpY1-#kr(B66a2#*^^ip0kslzAYqnTXvQrdqT9E"}
	encryptedMsg := CryptoString{"XSALSA20",
		"GD2~hHY+RnCbTb_!0~0Ig-Q4Npd(4`8}pc2<^3R-Y(z1D-KeWt5L8~BJhkPJ0o6>eACc?A" +
			"^#2)8*~vU5@ZNI^CC19^z!g3V*BJ@=FcdXpKo?x4*vN)X{(C~uQB}lklL5e4Mgvq2t#;46A<?wTYZ-#0>v" +
			"f<v6>|PiJdeKoHiJ@!v-#sx5O_>tM#Jl>q5"}

	// First, we'll instantiate an EncryptionPair. Note that it's normally a Really Bad Idea to
	// embed a key into source code like this, but this keypair is specifically for example use
	// only and is *never* used for real communications.
	aliceKeypair := EncryptionPairFromStrings(
		"CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`",
		"CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&")

	// Next, we'll decrypt the message key and create a SecretKey instance from it
	decryptedMsgKeyData, err := aliceKeypair.Decrypt(msgKeyData)
	if err != nil {
		panic(err)
	}

	// The key itself was a string in CryptoString format prior to being encrypted, so we can just
	// decrypt the key and then pass and cast.
	msgCS, err := NewCS(string(decryptedMsgKeyData))
	if err != nil {
		panic(err)
	}
	msgKey, err := NewSecretKey(msgCS)
	if err != nil {
		panic(err)
	}

	// Lastly, we'll decrypt the message and print it
	msg, err := msgKey.Decrypt(encryptedMsg)
	if err != nil {
		panic(err)
	}

	fmt.Printf("A secret message from Bob:\n\n%s\n", msg)
}

func TestSigningExample(t *testing.T) {

	// This example expands on the encryption example to include signing and verification so that
	// Alice can be sure that Bob actually sent the secret message and that it wasn't altered in
	// transit.

	// First, we instantiate a keypair used for signing and verification. Like the pair used in the
	// decryption example, this keypair is used only for examples and *never* for real
	// communications. Normally we would call GenerateSigningPair(), but we need to know these
	// values for the signature verification example.
	bobSigningPair := SigningPairFromStrings(
		"ED25519:PnY~pK2|;AYO#1Z;B%T$2}E$^kIpL=>>VzfMKsDx",
		"ED25519:{^A@`5N*T%5ybCU%be892x6%*Rb2rnYd=SGeO4jF")

	bobMessage := "Failures, repeated failures, are finger posts on the road to achievement. " +
		"One fails forward toward success. -- C.S. Lewis"

	bobSecretKey, err := GenerateSecretKey(PreferredSecretType())
	if err != nil {
		panic(err)
	}

	// Unlike before, we will sign the message. This means that Alice can be certain no one changed
	// the message in transit. While this might seem highly unnecessary, the worlds of cryptography
	// and cybersecurity are full of paranoia and, sadly, needs to be that way.
	//
	// Note that although you *could* encrypt the message and then sign it, it creates the
	// possibility, however remote, that someone could intercept the encrypted message, alter it,
	// replace the signature with their own, and send the altered message to Alice. Sign and then
	// encrypt, my friends. :)
	bobSignature, err := bobSigningPair.Sign([]byte(bobMessage))
	if err != nil {
		panic(err)
	}

	// We'll attach the signature to the end of the message so that Alice can easily split the
	// two apart and use the signature to verify the message. There are lots of ways to include
	// a signature with the signed data, and this is just one of them.
	msgWithSig := bobMessage + "\n" + bobSignature.AsString()
	encryptedMessage, err := bobSecretKey.Encrypt([]byte(msgWithSig))
	if err != nil {
		panic(err)
	}

	aliceEncCS, err := NewCS("CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`")
	if err != nil {
		panic(err)
	}

	aliceEncKey, err := NewEncryptionKey(aliceEncCS)
	if err != nil {
		panic(err)
	}

	encryptedKey, err := aliceEncKey.Encrypt(bobSecretKey.Key.AsBytes())
	if err != nil {
		panic(err)
	}

	fmt.Printf("The encrypted message key:\n%s\n\n", encryptedKey)
	fmt.Printf("Bob's secret message:\n%s\n", encryptedMessage)
}

func TestVerificationExample(t *testing.T) {

	// This example builds upon the signing example to demonstrate message verification

	msgKeyData := CryptoString{"CURVE25519",
		"+Forp*~nLUfuWP5BY#k`ry(M9%h6r*A#~y~d)Sy~9TWX^$(eV*wI~p5!cgpQJ1gX41p3L_bw" +
			"uKWMplC4WUG|+eLBR@qv?otXsWLY5_DD2jp(d?5U-qeS@1`^"}
	encryptedMsg := CryptoString{"XSALSA20",
		"Dlc4nE35B0x^ngZ`f$%*sw692LgnZKZyu-LyRwC)=`W)*@{+pH(G*H4holrTpIl!=LDbG@" +
			"37ZT_gzE9!v$?HoN8#rhv5ThqgYlXUF}>BUjm41tCH7U0+tjc71pgx>QsKZL@CE`C7Ky@vC{+GP0es3$J7" +
			"6;!ziimWjMw$Y={Wf|qx8{~hKmP(ejbwP<%KB)fTC6QlmJ&^+0KAKhn*rKOpm__*UjM>{;y`$DsCXx##2l" +
			"+ANbA1m4b7zf}(a*>TY#%6Yug~wSC?+C~h2i1Thy-uWY#9cx8n7e#)?-15)$}K1c"}

	// This keypair isn't used anywhere else and was generated strictly for testing purposes only.
	// Because this example builds on top of the signing example, we actually instantiate the
	// encryption keypair from known values.
	aliceKeypair := EncryptionPairFromStrings(
		"CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`",
		"CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&")

	// Here we'll decrypt the message key and then use it to decrypt the secret message
	decryptedMsgKeyData, err := aliceKeypair.Decrypt(msgKeyData)
	if err != nil {
		panic(err)
	}
	// The key itself was a CryptoString when it was encrypted, so decrypt and cast to set up the
	// key for use.
	msgKeyCS, err := NewCS(string(decryptedMsgKeyData))
	if err != nil {
		panic(err)
	}

	msgKey, err := NewSecretKey(msgKeyCS)
	if err != nil {
		panic(err)
	}

	msgAndSig, err := msgKey.Decrypt(encryptedMsg)
	if err != nil {
		panic(err)
	}

	// We're not done just yet. Now we separate the message from the signature and verify using
	// the verification key that Bob gave to Alice. In this case lines[0] will contain the message
	// and lines[1] will have the signature in CryptoString format.
	lines := strings.SplitN(string(msgAndSig), "\n", 2)

	signature, err := NewCS(lines[1])
	if err != nil {
		panic(err)
	}

	bobVerCS, err := NewCS("ED25519:PnY~pK2|;AYO#1Z;B%T$2}E$^kIpL=>>VzfMKsDx")
	if err != nil {
		panic(err)
	}

	bobVerKey, err := NewVerificationKey(bobVerCS)
	if err != nil {
		panic(err)
	}

	isVerified, err := bobVerKey.Verify([]byte(lines[0]), signature)
	if err != nil {
		panic(err)
	}

	if isVerified {
		fmt.Printf("A _verified_ secret message from my friend Bob:\n\n%s\n", lines[0])
	} else {
		fmt.Printf("A secret message from someone pretending to be my friend Bob:\n\n%s\n",
			lines[0])
	}
}

func TestHashingExample(t *testing.T) {

	mySecretKey, err := GenerateSecretKey(PreferredSecretType())
	if err != nil {
		panic(err)
	}

	keyHash, err := GetHash(PreferredHashType(), mySecretKey.Key.AsBytes())
	if err != nil {
		panic(err)
	}

	fmt.Printf("My Secret Key:\n%s\n", mySecretKey.Key.AsString())
	fmt.Printf("Hash of my secret key:\n%s\n", keyHash.AsString())
}
