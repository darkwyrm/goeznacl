package goeznacl

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"

	"gitlab.com/darkwyrm/b85"
	"golang.org/x/crypto/nacl/secretbox"
)

// Returns a list of supported symmetric encryption algorithms
func GetSecretAlgorithms() []string {
	if CertifiedAlgorithmsRequired() {
		return []string{
			"AES-128",
			"AES-256",
		}
	}

	return []string{
		"XSALSA20",
		"AES-128",
		"AES-256",
	}
}

// SecretKey defines a symmetric encryption key
type SecretKey struct {
	Hash CryptoString
	Key  CryptoString
}

// NewSecretKey creates a new SecretKey object from a CryptoString of the key. If given a bad key
// or one which uses an unsupported algorithm, it will return nil.
func NewSecretKey(keyString CryptoString) (SecretKey, error) {
	var out SecretKey

	// All parameter validation is handled in Set
	if err := out.Set(keyString); err != nil {
		return out, err
	}

	return out, nil
}

// SecretKeyFromString creates a new SecretKey object from a raw string in CryptoString format. If
// given a bad key or one which uses an unsupported algorithm, it will return nil.
func SecretKeyFromString(keyString string) *SecretKey {
	var newkey SecretKey

	keyCS, err := NewCS(keyString)
	if err != nil {
		return nil
	}

	// All parameter validation is handled in Set
	if newkey.Set(keyCS) != nil {
		return nil
	}

	return &newkey
}

// GenerateSecretKey creates a new SecretKey object with a randomly-generated key using a
// cryptographically safe method. This will return nil if given an invalid encryption algorithm
func GenerateSecretKey(algorithm string) (SecretKey, error) {

	var out SecretKey

	if !isValidSecretKeyType(algorithm) {
		return out, ErrUnsupportedAlgorithm
	}

	var keyBytes []byte

	switch algorithm {
	case "AES-128":
		keyBytes = make([]byte, 16)
	case "XSALSA20", "AES-256":
		keyBytes = make([]byte, 32)
	}
	if _, err := rand.Read(keyBytes); err != nil {
		return out, err
	}

	var keyString CryptoString
	keyString.Prefix = algorithm
	keyString.Data = b85.Encode(keyBytes)

	// All parameter validation is handled in Set
	if err := out.Set(keyString); err != nil {
		return out, err
	}

	return out, nil
}

// GetEncryptionType returns the algorithm used by the key
func (key SecretKey) GetEncryptionType() string {
	return key.Key.Prefix
}

// GetType returns the type of key -- asymmetric or symmetric
func (key SecretKey) GetType() string {
	return "symmetric"
}

// Set assigns a CryptoString value to the SecretKey
func (key *SecretKey) Set(keyString CryptoString) error {

	if !isValidSecretKeyType(keyString.Prefix) {
		return ErrUnsupportedAlgorithm
	}

	key.Key = keyString

	var err error
	key.Hash, err = GetHash(preferredHashAlgorithm, keyString.AsBytes())

	return err
}

// Encrypt encrypts a byte slice using the internal key. It returns the resulting encrypted
// data as a Base85-encoded string that amounts to a CryptoString without the prefix.
func (key SecretKey) Encrypt(data []byte) (CryptoString, error) {
	var empty CryptoString
	if data == nil {
		return empty, nil
	}

	keyDecoded := key.Key.RawData()
	if keyDecoded == nil {
		return empty, b85.ErrDecodingB85
	}

	switch key.Key.Prefix {
	case "AES-128", "AES-256":
		blockCipher, err := aes.NewCipher(keyDecoded)
		if err != nil {
			return empty, err
		}

		gcm, err := cipher.NewGCM(blockCipher)
		if err != nil {
			return empty, err
		}

		var nonce [12]byte
		rand.Read(nonce[:])

		var out = make([]byte, 12)
		copy(out, nonce[:])
		out = gcm.Seal(out, nonce[:], data, nil)
		return CryptoString{key.Key.Prefix, b85.Encode(out)}, nil

	case "XSALSA20":
		var nonce [24]byte
		rand.Read(nonce[:])

		var keyAdapter [32]byte
		copy(keyAdapter[:], keyDecoded)

		var out = make([]byte, 24)
		copy(out, nonce[:])
		out = secretbox.Seal(out, data, &nonce, &keyAdapter)
		return CryptoString{key.Key.Prefix, b85.Encode(out)}, nil
	default:
		return empty, errors.New("bug: unhandled algorithm in SecretKey::Encrypt")
	}
}

// Decrypt decrypts a string of encrypted data which is Base85 encoded using the internal key.
func (key SecretKey) Decrypt(data CryptoString) ([]byte, error) {
	if !data.IsValid() {
		return nil, ErrInvalidCS
	}

	keyDecoded := key.Key.RawData()
	if keyDecoded == nil {
		return nil, errors.New("decoding error in symmetric key")
	}

	var keyAdapter [32]byte
	copy(keyAdapter[:], keyDecoded)

	decodedData, err := b85.Decode(data.Data)
	if err != nil {
		return nil, errors.New("decoding error in data")
	}

	switch key.Key.Prefix {
	case "AES-128", "AES-256":
		var nonce [12]byte
		copy(nonce[:], decodedData)

		blockCipher, err := aes.NewCipher(keyDecoded)
		if err != nil {
			return nil, err
		}

		gcm, err := cipher.NewGCM(blockCipher)
		if err != nil {
			return nil, err
		}

		decryptedData, err := gcm.Open(nil, nonce[:], decodedData[12:], nil)
		if err != nil {
			return nil, err
		}

		return decryptedData, nil

	case "XSALSA20":
		var nonce [24]byte
		copy(nonce[:], decodedData)

		decryptedData, ok := secretbox.Open(nil, decodedData[24:], &nonce, &keyAdapter)
		if !ok {
			return nil, ErrDecryptionFailure
		}

		return decryptedData, nil
	default:
		return nil, errors.New("bug: unhandled algorithm in SecretKey::Decrypt")
	}

}

func isValidSecretKeyType(algo string) bool {
	switch algo {
	case "XSALSA20", "AES-128", "AES-256":
		return true
	}

	return false
}
