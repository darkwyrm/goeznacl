package goeznacl

import (
	"testing"
)

func TestSignVerifyED25519(t *testing.T) {
	verkey := CryptoString{"ED25519", "PnY~pK2|;AYO#1Z;B%T$2}E$^kIpL=>>VzfMKsDx"}
	signkey := CryptoString{"ED25519", "{^A@`5N*T%5ybCU%be892x6%*Rb2rnYd=SGeO4jF"}
	keypair := NewSigningPair(verkey, signkey)

	testData := "This is some signing test data"

	signature, err := keypair.Sign([]byte(testData))
	if err != nil || !signature.IsValid() {
		t.Fatal("SigningPair.Sign() failed")
	}

	verified, err := keypair.Verify([]byte(testData), signature)
	if err != nil || !verified {
		t.Fatal("SigningPair.Verify() failed")
	}
}

func TestSignVerifyECDSA_P256(t *testing.T) {
	verkey := CryptoString{"ECDSA-P256-SHA256",
		"Fj+7Y1_vsJNX|V10R{*vhDgpm0|5sELI4D?$IenMIz^MUy4YTJ29yOk#GmUH<`(G*h@Ya_>IsF>9p&BGey94d" +
			"GjaBl3ImvP5VGpqe*OL-u2PE&=}9#"}
	signkey := CryptoString{"ECDSA-P256-SHA256",
		"FoA~x0RS))1_vsJNX|V10R{*vhDgpm0|5sFZ7^#B0RaRcuzuXtp-U~otAi>=36adtY;(W>%(sI?TqQ#*hGzg{p" +
			"+o~h00gkd&QdKpMU%C<*j{!9lm$7&pX(Op7U>CypQ73735C%e<=xqSr~0rnarTo61DJCVvg+G@{r(}YQi}`eNi_"}
	keypair := NewSigningPair(verkey, signkey)

	testData := "This is some signing test data"

	signature, err := keypair.Sign([]byte(testData))
	if err != nil || !signature.IsValid() {
		t.Fatal("SigningPair.Sign() failed")
	}

	verified, err := keypair.Verify([]byte(testData), signature)
	if err != nil || !verified {
		t.Fatal("SigningPair.Verify() failed")
	}
}
