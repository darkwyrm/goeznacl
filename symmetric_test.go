package goeznacl

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"testing"
	"time"

	"golang.org/x/crypto/nacl/secretbox"
)

var symmetricIterations = 1000

// Do a basic encrypt-decrypt test of all algorithms available
func TestEZCryptSymEncryptDecrypt(t *testing.T) {

	keys := []CryptoString{
		{"XSALSA20", "hlibDY}Ls{F!yG83!a#E$|Nd3?MQ@9G=Q{7PB(@O"},
		{"AES-128", "hlibDY}Ls{F!yG83!a#E"},
		{"AES-256", "hlibDY}Ls{F!yG83!a#E$|Nd3?MQ@9G=Q{7PB(@O"},
	}

	for _, keystring := range keys {
		secretkey, err := NewSecretKey(keystring)
		if err != nil {
			t.Fatalf("NewSecretKey failed: %s", err.Error())
		}

		testData := "This is some encryption test data"
		encryptedData, err := secretkey.Encrypt([]byte(testData))
		if err != nil {
			t.Fatal("SecretKey.Encrypt() failed")
		}

		decryptedRaw, err := secretkey.Decrypt(encryptedData)
		if err != nil || decryptedRaw == nil {
			t.Fatal("SecretKey.Decrypt() failed")
		}

		if string(decryptedRaw) != testData {
			t.Fatal("SecretKey decrypted data mismatch")
		}
	}

}

func TestAES_128(t *testing.T) {

	testBuffer := make([]uint8, 1048576)
	key := make([]uint8, 16)

	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	gcm, err := cipher.NewGCM(blockCipher)
	if err != nil {
		panic(err)
	}

	var nonce [12]byte
	rand.Read(nonce[:])

	start := time.Now()
	for i := 0; i < symmetricIterations; i++ {

		var out = make([]byte, 12)
		copy(out, nonce[:])
		_ = gcm.Seal(out, nonce[:], testBuffer, nil)
	}
	elapsed := time.Since(start)
	t.Logf("AES-128: %s", elapsed)
}

func TestAES_256(t *testing.T) {

	testBuffer := make([]uint8, 1048576)
	key := make([]uint8, 32)

	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	gcm, err := cipher.NewGCM(blockCipher)
	if err != nil {
		panic(err)
	}

	var nonce [12]byte
	rand.Read(nonce[:])

	start := time.Now()
	for i := 0; i < symmetricIterations; i++ {

		var out = make([]byte, 12)
		copy(out, nonce[:])
		_ = gcm.Seal(out, nonce[:], testBuffer, nil)
	}
	elapsed := time.Since(start)
	t.Logf("AES-256: %s", elapsed)
}

func TestXSalsa20(t *testing.T) {

	testBuffer := make([]uint8, 1048576)
	key := make([]uint8, 32)

	var nonce [24]byte
	rand.Read(nonce[:])

	var keyAdapter [32]byte
	copy(keyAdapter[:], key)

	start := time.Now()
	for i := 0; i < symmetricIterations; i++ {

		var out = make([]byte, 24)
		copy(out, nonce[:])
		_ = secretbox.Seal(out, testBuffer, &nonce, &keyAdapter)
	}
	elapsed := time.Since(start)
	t.Logf("XSALSA20: %s", elapsed)
}
