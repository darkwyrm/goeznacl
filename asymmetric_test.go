package goeznacl

import (
	"testing"
)

func TestCurve25519(t *testing.T) {

	pubkey, err := NewCS("CURVE25519:(B2XX5|<+lOSR>_0mQ=KX4o<aOvXe6M`Z5ldINd`")
	if err != nil {
		t.Fatalf("CryptoString set failure: %s", err.Error())
	}

	privkey, err := NewCS("CURVE25519:(Rj5)mmd1|YqlLCUP0vE;YZ#o;tJxtlAIzmPD7b&")
	if err != nil {
		t.Fatalf("CryptoString set failure: %s", err.Error())
	}

	keypair, err := NewEncryptionPair(pubkey, privkey)
	if err != nil {
		t.Fatal("Keypair creation failure")
	}

	testData := "This is some encryption test data"
	encryptedData, err := keypair.Encrypt([]byte(testData))
	if err != nil {
		t.Fatal("EncryptedPair.Encrypt() failed")
	}

	decryptedRaw, err := keypair.Decrypt(encryptedData)
	if err != nil || decryptedRaw == nil {
		t.Fatal("EncryptedPair.Decrypt() failed")
	}

	if string(decryptedRaw) != testData {
		t.Fatal("EncryptedPair decrypted data mismatch")
	}

}

func TestRSA(t *testing.T) {

	// There are a lot of reasons why we avoid RSA where possible. Here's one:
	pubkey, err := NewCS("RSA2048-SHA256:FoFRhFbxI?Duzgg_YDC70R;d9f&mWzFoFRJ0)hbn0Ivtsg(Op_d;Lp~P(-19" +
		"e7SD~->7P(S{aPe5*GN?c|gq=gXLtuDc+a%X!9kLrNPFjOtN}uuwBQY-?Mq6FRV+)sp4lB^z&#1{fb5u#Zr4@V4" +
		"VM<ra?Mx=>Sd@@DW6A{(1T7%6ckh$Mv#m`cxqOy66?#fKyEML|CWjoaxonyXm>aY6Z??xD({_^wA*32xZrIl!M!" +
		"<Ndwbz-4fR-j1VJ#2rxJPgv?ZY^t$sZ-%_;X&XC?Iny08U`#;Qp52$ua@bz|X5n)2HzdWP^DXA6yP#_m7>Q<<dU" +
		"k4t1HBT0?m6slov$pI?W1!*y!Wgzt;v5N3Gls&AaRLJY009")
	if err != nil {
		t.Fatalf("CryptoString set failure: %s", err.Error())
	}
	privkey, err := NewCS("RSA2048-SHA256:FoFcX0s#Op4F(A+hDe6@4FLfG1pows1gS8B1f>E200M#m0RXQD)P*Edr+fW" +
		"Nk5ELReSEoZ1mCD?rCJ$`(h?T<)p<b87=z_xzbW3A_Gt4ZlcmANsZ6qZX|P?#q2IH4qc5yW$Eo6H7xeRJ1^tRf6" +
		"va||WMG{Cp{7APZs`C{74Q*6ZvJ`s>B@R4X2<oiYWh?l{krHC+kjI{^+Z^w=$z@*)Vt}q#A*f3W4IIK^Yqam#t3" +
		"ECca($Ms!0RWa@`WwDvS^#e+V!){)Eg_eDu2WD&JDH<j#=ZDVnFKGy6ZxfDfp4OYrq}ZV_QZvA;Z|11YH${!kzn" +
		"D(Y6Kl3xcNd^JxNv6Yt|k+Zh!N@Jkn0KyoyP~sd3P&0<Yjd2140RRC4f&l>lh=}kcC^1ETI2oWK`c{h|CpykAb{" +
		";bPbl>0O5}s>N8P}tq2kWjMh%Y>yqsEm*fkJ9Pl^tH9pp_ui^B5S<5Oq+zS_>Dd5z+jTN-^H_3L=dwyllYvg8GF" +
		"RClA7xF|iYHSx_}9s9(7&l%nWHd?)-RxATnku7zUqJi<nH4xL0wuIYm|ty&+X2!|0jrrC<~N5w%UO*Il<ys|W@>" +
		"ui)oR@KGHeD_GuqjdRlR?s}&G-84(RS@+h0Emb-2K#_jajDQI|LNInrbEF#<A&lX4Iotc1?b^)Z4AzXtT!Z1za&" +
		"svx~8Gu)@&{T*MNUy5{{=x)KQE7v0Zur0)c@5+GZcv;SX7;q@xu8^|Sv7WQs7BB$=O|143)^*@oV$!i@1YB9))F" +
		"I8S-Cp$oU+7+VTC!)iO||Eu8Y?4u6K0$3U)N!8mF`hHluMsMjb_~EiE;_r-DbiFgOh^JrZpdP{K#R{w3Wil{SM>" +
		"I(u6rE`5g67+0tm)yx_YU-t0)c@5%_>3j0l^yrZ$MZYh!+X{+T^5V2~SQYB0AM)oN_YT3D;~tWWFs%_%{LvC@bh" +
		"H`{V)}%h_SGQV8^_P-7N+$X@o_*TM36q+)g~t3!FPTWW8PzTlWTY~l&r2X_U4qnjg&_50?Jv)rlzdg&K+zAM@JM" +
		"z9e`a2KSak4XCA0)c=A$n9)fagw8cnc`_FIPy<ia6XzVfa*w+U0LNVgx4Mx=`sZ{h23;tw|HEDC{YR-E1KcBgm7" +
		"R8v06uR8jiOQ<CM%>!rc#r$gdFsTQ9F^%lR=2*UeC3Fb<CySGms&N@!EY>rwN9;ZR2n|2@`4Wv+m_m|1X(GN|O)" +
		"un9o|fq?*!?Q`zTV{^yxe7k?@c$4Q&G)rjD{lt&0h;Qwr7YmcEO%A(lSLQYb>m0P7jQo)}q@`4=SYYbHU2iYUUt" +
		"*{|2GC6w34B(sXPRLRjybIPwxb<u`pvE%@<7tOH#nTxb)wSi*2vW4Qc;RD_UTHLPWQ-OMa#dY=Nz_lk1`<wfq?*" +
		"tE8)DhE8^<kMoRG00Wv(IVxU65+xed8Jl}}yTQdgIo1WVU*0+UAHKuq*&qM^D&#GYQnujdc=)Ju24{$wcq<@KLe" +
		"Be6>)yzo*x7HxUZZ_m%sF0Z4LWDG~Mn6>OmmaB%s$0I!7p|ny_c7bm!FgmsjwDno-Nbm{;2xs")
	if err != nil {
		t.Fatalf("CryptoString set failure: %s", err.Error())
	}

	keypair, err := NewEncryptionPair(pubkey, privkey)
	if err != nil {
		t.Fatal("Keypair creation failure")
	}

	testData := "This is some encryption test data"
	encryptedData, err := keypair.Encrypt([]byte(testData))
	if err != nil {
		t.Fatal("EncryptedPair.Encrypt() failed")
	}

	decryptedRaw, err := keypair.Decrypt(encryptedData)
	if err != nil || decryptedRaw == nil {
		t.Fatal("EncryptedPair.Decrypt() failed")
	}

	if string(decryptedRaw) != testData {
		t.Fatal("EncryptedPair decrypted data mismatch")
	}
}
