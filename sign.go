package goeznacl

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"errors"
	"fmt"

	"gitlab.com/darkwyrm/b85"
)

// Returns a list of supported signing algorithms
func GetSigningAlgorithms() []string {
	if CertifiedAlgorithmsRequired() {
		return []string{
			"ECDSA-P256-SHA256",
		}
	}

	return []string{
		"ED25519",
		"ECDSA-P256-SHA256",
	}
}

// VerificationKey is an object to represent just a verification key, not a key pair
type VerificationKey struct {
	PublicHash CryptoString
	key        CryptoString
}

// NewVerificationKey creates a new verification key from a CryptoString
func NewVerificationKey(key CryptoString) (VerificationKey, error) {
	var out VerificationKey
	if err := out.Set(key); err != nil {
		return out, err
	}

	return out, nil
}

// VerificationKeyFromString creates a new verification key from a string containing data in
// CryptoString format. This call is best used in situations where the data is known-good. Any
// error will cause this call to return nil.
func VerificationKeyFromString(key string) *VerificationKey {
	var out VerificationKey

	keyCS, err := NewCS(key)
	if err != nil {
		return nil
	}
	if err := out.Set(keyCS); err != nil {
		return nil
	}

	return &out
}

// GetEncryptionType returns the algorithm used by the key
func (vkey VerificationKey) GetEncryptionType() string {
	return vkey.key.Prefix
}

// GetType returns the type of key -- asymmetric or symmetric
func (vkey VerificationKey) GetType() string {
	return "asymmetric"
}

// Verify uses the internal verification key with the passed data and signature and returns true
// if the signature has verified the data with that key.
func (vkey VerificationKey) Verify(data []byte, signature CryptoString) (bool, error) {
	if !signature.IsValid() {
		return false, errors.New("invalid signature")
	}

	if !isValidSigningKeyType(signature.Prefix) {
		return false, ErrUnsupportedAlgorithm
	}
	digest := signature.RawData()
	if digest == nil {
		return false, b85.ErrDecodingB85
	}

	verifyKeyDecoded := vkey.key.RawData()
	if verifyKeyDecoded == nil {
		return false, b85.ErrDecodingB85
	}

	switch vkey.key.Prefix {
	case "ED25519":
		return ed25519.Verify(verifyKeyDecoded, data, digest), nil
	case "ECDSA-P256-SHA256":
		keyAdapter, err := x509.ParsePKIXPublicKey(verifyKeyDecoded)
		if err != nil {
			return false, fmt.Errorf("error decoding verification key: %s", err.Error())
		}
		verifyKey := keyAdapter.(*ecdsa.PublicKey)

		hash := sha256.Sum256(data)

		return ecdsa.VerifyASN1(verifyKey, hash[:], digest), nil
	default:
		return false, errors.New("bug: unhandled algorithm in VerificationKey::Verify()")
	}
}

// Set assigns a CryptoString value to the key
func (vkey *VerificationKey) Set(key CryptoString) error {
	if !isValidSigningKeyType(key.Prefix) {
		return ErrUnsupportedAlgorithm
	}
	vkey.key = key

	var err error
	vkey.PublicHash, err = GetHash(preferredHashAlgorithm, vkey.key.AsBytes())

	return err
}

// SigningPair defines an asymmetric signing key pair
type SigningPair struct {
	PublicHash  CryptoString
	PrivateHash CryptoString
	PublicKey   CryptoString
	PrivateKey  CryptoString
}

// NewSigningPair creates a new SigningPair object from two CryptoString objects
func NewSigningPair(pubkey CryptoString, privkey CryptoString) *SigningPair {
	var newpair SigningPair

	// All parameter validation is handled in Set
	if newpair.Set(pubkey, privkey) != nil {
		return nil
	}

	return &newpair
}

// SigningPairFromStrings creates a new SigningPair object from two strings containing data in
// CryptoString format. This call is intended for use in situations where you know the data is
// valid. If any problems are found, nil is returned.
func SigningPairFromStrings(pubstr string, privstr string) *SigningPair {
	var out SigningPair

	pubkey, err := NewCS(pubstr)
	if err != nil {
		return nil
	}

	privkey, err := NewCS(privstr)
	if err != nil {
		return nil
	}

	// All parameter validation is handled in Set
	if err := out.Set(pubkey, privkey); err != nil {
		return nil
	}

	return &out
}

// GetEncryptionType returns the algorithm used by the key
func (spair SigningPair) GetEncryptionType() string {
	return spair.PublicKey.Prefix
}

// GetType returns the type of key -- asymmetric or symmetric
func (spair SigningPair) GetType() string {
	return "signing"
}

// Set assigns a pair of CryptoString values to the SigningPair
func (spair *SigningPair) Set(pubkey CryptoString, privkey CryptoString) error {

	if pubkey.Prefix != privkey.Prefix {
		return errors.New("key type mismatch")
	}

	if !isValidSigningKeyType(pubkey.Prefix) || !isValidSigningKeyType(privkey.Prefix) {
		return ErrUnsupportedAlgorithm
	}
	spair.PublicKey = pubkey
	spair.PrivateKey = privkey

	var err error
	spair.PublicHash, err = GetHash(preferredHashAlgorithm, pubkey.AsBytes())
	if err != nil {
		return err
	}

	spair.PrivateHash, err = GetHash(preferredHashAlgorithm, privkey.AsBytes())
	return err
}

// GenerateSigningPair creates a new instance with a randomly-generated key pair
func GenerateSigningPair(algorithm string) (SigningPair, error) {

	var out SigningPair

	if !isValidSigningKeyType(algorithm) {
		return out, ErrUnsupportedAlgorithm
	}

	switch algorithm {
	case "ED25519":
		verkey, signkey, err := ed25519.GenerateKey(rand.Reader)
		if err != nil {
			return out, err
		}

		err = out.Set(CryptoString{"ED25519", b85.Encode(verkey[:])},
			CryptoString{"ED25519", b85.Encode(signkey.Seed())})
		if err != nil {
			return out, err
		}
	case "ECDSA-P256-SHA256":
		// Unlike the easy interface generated by the ed25519 module, ECDSA keys are a bit more
		// complicated. Once generated, we'll marshal them into ASN.1 DER form and then encode them
		privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		if err != nil {
			return out, err
		}

		flatPriv, err := x509.MarshalPKCS8PrivateKey(privateKey)
		if err != nil {
			return out, err
		}

		flatPub, err := x509.MarshalPKIXPublicKey(&privateKey.PublicKey)
		if err != nil {
			return out, err
		}

		err = out.Set(CryptoString{"ECDSA-P256-SHA256", b85.Encode(flatPub)},
			CryptoString{"ECDSA-P256-SHA256", b85.Encode(flatPriv)})
		if err != nil {
			return out, err
		}
	default:
		return out, errors.New("bug: unhandled algorithm in GenerateSigningPair()")
	}

	return out, nil
}

// Sign cryptographically signs a byte slice.
func (spair SigningPair) Sign(data []byte) (CryptoString, error) {
	var out CryptoString

	signkeyDecoded := spair.PrivateKey.RawData()
	if signkeyDecoded == nil {
		return out, errors.New("bad signing key")
	}

	switch spair.PrivateKey.Prefix {
	case "ED25519":
		// We bypass the nacl/sign module because it requires a 64-bit private key. We, however,
		// pass around the 32-bit ed25519 seeds used to generate the keys. Thus, we have to skip
		// using nacl.Sign() and go directly to the equivalent code in the ed25519 module.
		signKeyPriv := ed25519.NewKeyFromSeed(signkeyDecoded)
		signature := ed25519.Sign(signKeyPriv, data)
		out.SetFromBytes("ED25519", signature)

	case "ECDSA-P256-SHA256":
		keyAdapter, err := x509.ParsePKCS8PrivateKey(signkeyDecoded)
		if err != nil {
			return out, fmt.Errorf("error decoding signing key: %s", err.Error())
		}
		privKey := keyAdapter.(*ecdsa.PrivateKey)
		hash := sha256.Sum256(data)

		rawSig, err := ecdsa.SignASN1(rand.Reader, privKey, hash[:])
		if err != nil {
			return out, fmt.Errorf("error signing data: %s", err.Error())
		}

		out.SetFromBytes("ECDSA-P256-SHA256", rawSig)

	default:
		return out, errors.New("bug: unhandled algorithm in Sign()")
	}

	return out, nil
}

// Verify uses the internal verification key with the passed data and signature and returns true
// if the signature has verified the data with that key.
func (spair SigningPair) Verify(data []byte, signature CryptoString) (bool, error) {
	if !signature.IsValid() {
		return false, errors.New("invalid signature")
	}

	if !isValidSigningKeyType(signature.Prefix) {
		return false, ErrUnsupportedAlgorithm
	}
	digest := signature.RawData()
	if digest == nil {
		return false, b85.ErrDecodingB85
	}

	verifyKeyDecoded := spair.PublicKey.RawData()
	if verifyKeyDecoded == nil {
		return false, b85.ErrDecodingB85
	}

	switch spair.PrivateKey.Prefix {
	case "ED25519":
		return ed25519.Verify(verifyKeyDecoded, data, digest), nil
	case "ECDSA-P256-SHA256":
		keyAdapter, err := x509.ParsePKIXPublicKey(verifyKeyDecoded)
		if err != nil {
			return false, fmt.Errorf("error decoding verification key: %s", err.Error())
		}
		verifyKey := keyAdapter.(*ecdsa.PublicKey)

		hash := sha256.Sum256(data)

		return ecdsa.VerifyASN1(verifyKey, hash[:], digest), nil
	default:
		return false, errors.New("bug: unhandled algorithm in GenerateSigningPair()")
	}

}

func isValidSigningKeyType(algo string) bool {
	switch algo {
	case "ED25519", "ECDSA-P256-SHA256":
		return true
	}

	return false
}
