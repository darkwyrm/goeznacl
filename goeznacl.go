package goeznacl

import (
	"errors"
)

// This module creates some types which make working with encryption in Go much less difficult.
// It recommends certain algorithms for each use case, but also allows the user to choose others
// and even helps know which ones are certified for use in situations that specify algorithms for
// compliance purposes (FIPS, etc.).

var ErrUnsupportedAlgorithm = errors.New("unsupported algorithm")
var ErrDecryptionFailure = errors.New("decryption failure")
var ErrVerificationFailure = errors.New("verification failure")

// EncryptorKey defines an interface for encrypting data to a CryptoString.
type EncryptorKey interface {
	Encrypt(data []byte) (CryptoString, error)
}

// DecryptorKey defines an interface for decrypting data in CryptoString format back to its raw
// form.
type DecryptorKey interface {
	Decrypt(data CryptoString) ([]byte, error)
}

// The CryptoKey interface defines methods to identify the different kinds of keys defined in this
// module.
type CryptoKey interface {
	GetEncryptionType() string
	GetType() string
}

var useCertifiedAlgorithms = false
var preferredHashAlgorithm = "BLAKE3-256"
var preferredSecretAlgorithm = "AES-256"
var preferredSigningAlgorithm = "ED25519"
var preferredAsymmetricAlgorithm = "CURVE25519"

// Returns the name of the library's recommended secret key algorithm
func PreferredSecretType() string {
	return preferredSecretAlgorithm
}

// Returns the name of the library's recommended hash algorithm
func PreferredHashType() string {
	return preferredHashAlgorithm
}

// Returns the name of the library's recommended signing algorithm
func PreferredSigningType() string {
	return preferredSigningAlgorithm
}

// Returns the name of the library's recommended asymmetric encryption algorithm
func PreferredAsymmetricType() string {
	return preferredAsymmetricAlgorithm
}

// Tells the library to require (or not) certified algorithms, which defaults to not requiring them.
// Unless you have specific requirements to use this, don't. The recommended choices provide a
// balance of ease of use, speed, key size, and interoperability.
func RequireCertifiedAlgorithms(require bool) {
	useCertifiedAlgorithms = require

	// XSalsa20, while good, doesn't benefit from hardware acceleration. Most of the uses of this
	// library will be in situations where AES will be faster because of being run on processors
	// which provide hardware acceleration for AES operations.
	if require {
		preferredHashAlgorithm = "SHA-256"
		preferredSigningAlgorithm = "ECDSA-P256-SHA256"
		preferredAsymmetricAlgorithm = "RSA2048-SHA256"
	} else {
		preferredHashAlgorithm = "BLAKE3-256"
		preferredSigningAlgorithm = "ED25519"
		preferredAsymmetricAlgorithm = "CURVE25519"
	}
}

// Returns true if the library is using only certified algorithms
func CertifiedAlgorithmsRequired() bool {
	return useCertifiedAlgorithms
}

// Returns true if the requested algorithm is one of the certified algorithms supported by the
// library. For those curious, this is as follows:
//
// RSA2048-SHA256: 2048-bit RSA with SHA256 for internal hashing. PKCS #1, RFC 8017, and an
// allowed (but not approved) algorithm for key transport in FIPS 140-2.
//
// ECDSA: Digital signatures using elliptic curve P-256 and SHA256 for internal hashing. FIPS 186-4
// and SEC 1, Version 2.0
//
// AES-128, AES-256: FIPS Publication 197.
//
// SHA-256, SHA-512: 256-bit and 512-bit variants of the SHA2 hashing algorithm as documented in
// FIPS 180-4.
//
// SHAKE-256, SHAKE-512: The SHAKE128 and SHAKE256 hashing algorithms, published in FIPS
// publication 202. Note that these algorithms require a 2x hash length to provide the
// corresponding 128 bits and 256 bits of security. The goeznacl prefixes denote the bit length of
// the hash, not the bits of security granted.
//
// The ED25519 signature algorithm is an approved digital signature algorithm in the current draft
// of FIPS 186-5, but because the draft has not received final approval, it is not in this list.
//
// As far as I am aware, there is no mention of Curve25519 asymmetric encryption as being allowed
// for key transport, but it is possible that it will achieve this status at some point in the
// future.
func IsCertifiedAlgorithm(algorithm string) bool {

	switch algorithm {
	// Asymmetric encryption
	case "RSA2048-SHA256":
		return true

	// Signatures
	case "ECDSA-P256-SHA256":
		return true

	// Symmetric encryption
	case "AES-128", "AES-256":
		return true

	// Hashing
	case "SHA-256", "SHA-512", "SHAKE-256", "SHAKE-512":
		return true
	}

	return false
}
