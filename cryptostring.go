package goeznacl

import (
	"errors"
	"regexp"
	"strings"

	"gitlab.com/darkwyrm/b85"
)

// This module contains the Go implementation of CryptoString.

// CryptoString is a compact way of handling cryptographic hashes, signatures, and keys such that
// (1) the algorithm used is obvious and (2) the data is encoded as text. The RFC 1924 variant of
// Base85 encoding is used because it is more compact than Base64 and friendly to source code.

// The format looks like this: ALGORITHM:xxxxxxxxxxxxxxxxxxxx, where ALGORITHM is the name of the
// algorithm and the Xs represent the Base85-encoded data. The prefix is limited to 24 characters
// including the colon separator. Only capital ASCII letters, numbers, and dashes may be used in
// the prefix.
//
// Examples:
//	- BLAKE3-256:^zPiV;CKvLd2(uwpIzmyMotYFsKM=cgbL=nSI2LN
//	- ED25519:6lXjej0C~!F&_`qnkPHrC`z8+>;#g*fNfjV@4ngGlp#xsr8}1rS2(NG
//

var reFormatPattern = regexp.MustCompile("^[A-Z0-9-]{1,24}")

var ErrInvalidCS = errors.New("invalid cryptostring")

type CryptoString struct {
	Prefix string
	Data   string
}

// NewCS generates an instance from a string containing data in CryptoString format
func NewCS(str string) (CryptoString, error) {
	var out CryptoString
	if err := out.Set(str); err != nil {
		return out, err
	}
	return out, nil
}

// NewFromBytes creates a CryptoString object from an algorithm and buffer of raw data. If you have
// an existing key or hash, this is probably what you want to use.
func NewCSFromBytes(algorithm string, buffer []byte) (CryptoString, error) {
	var out CryptoString
	if err := out.SetFromBytes(algorithm, buffer); err != nil {
		return out, err
	}
	return out, nil
}

// Set takes a CryptoString-formatted string and sets the object to it.
func (cs *CryptoString) Set(str string) error {
	cs.Prefix = ""
	cs.Data = ""

	// Data checks

	if !reFormatPattern.MatchString(str) {
		return ErrUnsupportedAlgorithm
	}

	parts := strings.SplitN(str, ":", 2)
	if len(parts) != 2 || len(parts[1]) < 1 {
		return ErrInvalidCS
	}

	_, err := b85.Decode(parts[1])
	if err != nil {
		return b85.ErrDecodingB85
	}

	cs.Prefix = parts[0]
	cs.Data = parts[1]
	return nil
}

// SetFromBytes assigns an algorithm and the associated data to the object. The CryptoString object
// has its own copy of the data passed to it in encoded form.
func (cs *CryptoString) SetFromBytes(algorithm string, buffer []byte) error {

	if len(algorithm) > 0 {
		if !reFormatPattern.MatchString(algorithm) {
			return ErrInvalidCS
		}
		cs.Prefix = algorithm
	} else {
		cs.Prefix = ""
	}

	if buffer != nil {
		cs.Data = b85.Encode(buffer)
	} else {
		cs.Data = ""
	}

	return nil
}

// AsString returns the instance's prefix and encoded data as a string
func (cs *CryptoString) AsString() string {
	return cs.Prefix + ":" + cs.Data
}

// AsBytes returns the instance's prefix and encoded data as a byte slice
func (cs *CryptoString) AsBytes() []byte {
	return []byte(cs.Prefix + ":" + cs.Data)
}

// RawData returns the raw, unencoded data of the object as a byte slice. In the event of an error,
// nil is returned
func (cs *CryptoString) RawData() []byte {
	out, err := b85.Decode(cs.Data)
	if err != nil {
		return nil
	}
	return out
}

// MakeEmpty returns the object to an uninitialized state
func (cs *CryptoString) MakeEmpty() {
	cs.Prefix = ""
	cs.Data = ""
}

// IsValid checks the internal data and returns True if it is valid
func (cs *CryptoString) IsValid() bool {
	if !reFormatPattern.MatchString(cs.Prefix) {
		return false
	}

	if len(cs.Data) < 1 {
		return false
	}

	if _, err := b85.Decode(cs.Data); err != nil {
		return false
	}

	return true
}
