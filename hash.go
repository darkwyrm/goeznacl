package goeznacl

import (
	"crypto/sha256"
	"crypto/sha512"

	"github.com/zeebo/blake3"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/sha3"
)

// Returns a list of supported hash algorithms
func GetHashAlgorithms() []string {

	if CertifiedAlgorithmsRequired() {
		return []string{
			"SHA-256",
			"SHA-512",
			"SHAKE-256",
			"SHAKE-512",
		}
	}

	return []string{
		"BLAKE2B-256",
		"BLAKE2B-512",
		"BLAKE3-256",
		"SHA-256",
		"SHA-512",
		"SHAKE-256",
		"SHAKE-512",
	}
}

// GetHash generates a CryptoString hash of the supplied data
func GetHash(algorithm string, data []byte) (CryptoString, error) {
	var out CryptoString

	hashdone := false
	if !CertifiedAlgorithmsRequired() {
		switch algorithm {
		case "BLAKE2B-256":
			rawHash := blake2b.Sum256(data)
			out.SetFromBytes(algorithm, rawHash[:])
			hashdone = true
		case "BLAKE2B-512":
			rawHash := blake2b.Sum512(data)
			out.SetFromBytes(algorithm, rawHash[:])
			hashdone = true
		case "BLAKE3-256":
			rawHash := blake3.Sum256(data)
			out.SetFromBytes(algorithm, rawHash[:])
			hashdone = true
		}
		if hashdone {
			return out, nil
		}
	}

	switch algorithm {
	case "SHA-256":
		rawHash := sha256.Sum256(data)
		out.SetFromBytes(algorithm, rawHash[:])
	case "SHA-512":
		rawHash := sha512.Sum512(data)
		out.SetFromBytes(algorithm, rawHash[:])
	case "SHAKE-256":
		rawHash := make([]byte, 32)
		sha3.ShakeSum128(rawHash, data)
		out.SetFromBytes(algorithm, rawHash[:])
	case "SHAKE-512":
		rawHash := make([]byte, 64)
		sha3.ShakeSum256(rawHash, data)
		out.SetFromBytes(algorithm, rawHash[:])
	default:
		return out, ErrUnsupportedAlgorithm
	}
	return out, nil
}

// CheckHash generates a CryptoString hash of the supplied data
func CheckHash(hash CryptoString, data []byte) (bool, error) {

	var test CryptoString
	hashdone := false
	if !CertifiedAlgorithmsRequired() {
		switch hash.Prefix {
		case "BLAKE2B-256":
			rawHash := blake2b.Sum256(data)
			test.SetFromBytes(hash.Prefix, rawHash[:])
			hashdone = true
		case "BLAKE2B-512":
			rawHash := blake2b.Sum512(data)
			test.SetFromBytes(hash.Prefix, rawHash[:])
			hashdone = true
		case "BLAKE3-256":
			rawHash := blake3.Sum256(data)
			test.SetFromBytes(hash.Prefix, rawHash[:])
			hashdone = true
		}
		if hashdone {
			return test.AsString() == hash.AsString(), nil
		}
	}

	switch hash.Prefix {
	case "SHA-256":
		rawHash := sha256.Sum256(data)
		test.SetFromBytes(hash.Prefix, rawHash[:])
	case "SHA-512":
		rawHash := sha512.Sum512(data)
		test.SetFromBytes(hash.Prefix, rawHash[:])
	case "SHAKE-256":
		rawHash := make([]byte, 32)
		sha3.ShakeSum128(rawHash, data)
		test.SetFromBytes(hash.Prefix, rawHash[:])
	case "SHAKE-512":
		rawHash := make([]byte, 64)
		sha3.ShakeSum256(rawHash, data)
		test.SetFromBytes(hash.Prefix, rawHash[:])
	default:
		return false, ErrUnsupportedAlgorithm
	}

	return test.AsString() == hash.AsString(), nil
}
