package goeznacl

import (
	"testing"
)

func TestArgonHashing(t *testing.T) {
	sampleHash := "$argon2id$v=19$m=65536,t=2," +
		"p=1$azRGvkBtov+ML8kPEaBcIA$tW+o1B8XeLmbKzzN9EQHu9dNLW4T6ia4ha0Z5ZDh7f4"
	ok, err := IsArgonHash(sampleHash)
	if err != nil || !ok {
		t.Fatal("IsArgonHash rejected a valid Argon2 hash")
	}

	testHash := HashPassword("MyS3cretPassw*rd")
	ok, err = IsArgonHash(testHash)
	if err != nil || !ok {
		t.Fatal("HashPassword created a bad hash or had an error")
	}

	ok, err = VerifyPasswordHash("MyS3cretPassw*rd", sampleHash)
	if err != nil || !ok {
		t.Fatal("VerifyPasswordHash created a bad hash or had an error")
	}

	ok, _ = VerifyPasswordHash("BadPassw*rd", sampleHash)
	if ok {
		t.Fatal("VerifyPasswordHash failed to reject a password mismatch")
	}
}
